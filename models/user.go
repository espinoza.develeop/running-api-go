package models

import (
	"github.com/gofiber/fiber"
	"github.com/jinzhu/gorm"
	"github.com/miposdev/running-api-go/config/database"
)

type User struct {
	gorm.Model
	Name            string `json:"name"`
	Email           string `json:"email"`
	AppleIdentifier string `json:"apple_identifier"`
	DeviceId        string `json:"device_id"`
	DeviceIdPhone   string `json:"device_id_phone"`
}

func GetUserByEmailAppleId(email string, appleId string) User {
	db := database.DBconn
	var user User
	db.Where("email = ? AND apple_identifier = ?", email, appleId).First(&user)
	return user
}

func GetUserByAppleId(appleId string) User {
	db := database.DBconn
	var user User
	db.Where("apple_identifier = ?", appleId).First(&user)
	return user
}

func GetUserById(id int) User {
	db := database.DBconn
	var user User
	db.Where("id = ?", id).First(&user)
	return user
}

func NewUser(c *fiber.Ctx) *User {
	db := database.DBconn
	user := new(User)
	c.BodyParser(user)
	db.Create(&user)
	return user
}
func UpdateDevice(user_id int, device_id string, device_id_phone string) User {
	db := database.DBconn
	var user User
	if device_id != ""{
		db.Model(&user).Where("id = ?", user_id).Update("device_id", device_id)
	}
	if device_id_phone != ""{
		db.Model(&user).Where("id = ?", user_id).Update("device_id_phone", device_id_phone)
	}
	
	return user
}


func UpdateEmail(user_id int, email string) User {
	db := database.DBconn
	var user User
	db.Model(&user).Where("id = ?", user_id).Update("email", email)
	return user
}
