package models

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"github.com/miposdev/running-api-go/config/database"
)

type RaceUser struct {
	gorm.Model
	UserId int  `json:"user_id"`
	RaceId int  `json:"race_id"`
	Active bool `json:"active"`
}

func NewRaceUser(raceUser *RaceUser) *RaceUser {
	db := database.DBconn
	db.Create(&raceUser)
	return raceUser
}

func GetUserRaceByUserId(user_id int, race_id int) RaceUser {
	db := database.DBconn
	var userRace RaceUser
	db.Where("user_id = ? AND race_id = ?", user_id, race_id).First(&userRace)
	return userRace
}

func GetLastUserRaceByUserId(user_id int) RaceUser {
	db := database.DBconn
	var userRace RaceUser
	db.Where("user_id = ? ", user_id).Last(&userRace)
	return userRace
}

type Result struct {
	UserId   		int     `json:"user_id"`
	Active   		bool    `json:"active"`
	Name     		string  `json:"name"`
	Email    		string  `json:"email"`
	DeviceId 		string  `json:"device_id"`
	DeviceIdPhone 	string  `json:"device_id"`
	Km       		float32 `json:"km"`
	Speed    		float32 `json:"speed"`
	Distance 		float32 `json:"distance"`
}

func GetUserRaceByRaceId(race_id int) []Result {
	db := database.DBconn

	var result []Result
	db.Raw("SELECT rc.user_id, rc.active, u.name, u.email, u.device_id, u.device_id_phone, 0.0 as km, 0.0 as speed, 0.0 as distance from race_users rc join users u on u.id = rc.user_id where rc.race_id  =? and rc.active=?", race_id, true).Scan(&result)
	//db.Where("race_id = ? AND active =?", race_id, true).Find(&userRace)
	return result
}

func UpdateRaceUserByUserId(user_id int, race_id int, status bool) RaceUser {
	fmt.Println("en el meotdo de update")
	fmt.Println(user_id)
	fmt.Println(race_id)
	fmt.Println(status)
	db := database.DBconn
	var raceUser RaceUser
	db.Model(&raceUser).Where("user_id = ? AND race_id = ?", user_id, race_id).Update("active", status)
	db.Where("user_id = ? AND race_id = ?", user_id, race_id).First(&raceUser)
	return raceUser
}
