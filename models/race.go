package models

import (
	"time"

	"github.com/jinzhu/gorm"
	"github.com/miposdev/running-api-go/config/database"
	"github.com/miposdev/running-api-go/utils"
)

type Race struct {
	gorm.Model
	UserId      	int       `json:"user_id"`
	RaceId      	string    `json:"race_id"`
	StrokeLength    int    	  `json:"stroke_length"`
	PaceStimated	float32   `json:"pace_stimated"; sql:"type:decimal(4,4);`
	Active      	bool      `json:"active"`
	Status      	string    `json:"status"`
	EnabledAt		time.Time `json:"enabled_at"`
	InitiatedAt 	time.Time `json:"initiated_at"`
	ScheduledAt 	time.Time `json:"scheduled_at"`
}

func NewRace(race *Race) *Race {
	db := database.DBconn
	db.Create(&race)
	return race
}

func GetRaceByRamdomId(id string) Race {
	db := database.DBconn
	var race Race
	db.Where("race_id = ?", id).First(&race)
	return race
}

func GetRaceById(id int) Race {
	db := database.DBconn
	var race Race
	db.Where("id = ?", id).First(&race)
	return race
}

func GetLastRaceByUserId(id int) Race {
	db := database.DBconn
	var race Race
	db.Where("user_id = ?", id).Last(&race)
	return race
}

func GetTenRaceByUserId(id int) []Race {
	db := database.DBconn
	var result []Race
	db.Raw("SELECT * from races  where user_id  = ? and status = 'E' and active = ? order by id desc limit ?", id, true, 10).Scan(&result)
	return result
}

func UpdateRaceByRamdomId(id string, status string) Race {
	db := database.DBconn
	var race Race
	if status == utils.RACE_INITIATED {
		dt := time.Now()
		//Format MM-DD-YYYY
		race.InitiatedAt = dt
		db.Model(&race).Where("race_id = ?", id).Updates(Race{Status: status, InitiatedAt: dt})
	}else if status == utils.RACE_CREATED{
		dt := time.Now()
		//Format MM-DD-YYYY
		race.EnabledAt = dt
		db.Model(&race).Where("race_id = ?", id).Updates(Race{Status: status, EnabledAt: dt})
	} else {
		db.Model(&race).Where("race_id = ?", id).Update("status", status)
	}

	db.Where("race_id = ?", id).First(&race)
	return race
}
