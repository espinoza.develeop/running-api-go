package models

import (
	"github.com/jinzhu/gorm"
	"github.com/miposdev/running-api-go/config/database"
)

type RaceData struct {
	gorm.Model
	RaceUserId int     `json:"race_user_id"`
	Latitude   float64 `json:"latitude"; sql:"type:decimal(9,6);`
	Longitude  float64 `json:"longitude";  sql:"type:decimal(9,6);`
	Speed      float32 `json:"speed"; sql:"type:decimal(4,4);`
	Altitude   float32 `json:"altitude"; sql:"type:decimal(6,4);`
	HeartRate  float32 `json:"heart_rate" ; sql:"type:decimal(4,4);`
	Distance   float64 `json:"distance" ; sql:"type:decimal(4,4);`
	Active     bool    `json:"active"`
}

func NewRaceData(raceData *RaceData) *RaceData {
	db := database.DBconn
	db.Create(&raceData)
	return raceData
}
func GetRaceDataLast(race_user_id int) []RaceData {
	db := database.DBconn

	var result []RaceData
	db.Raw("SELECT * from race_data  where race_user_id  = ? and active=? order by id desc limit 1", race_user_id, true).Scan(&result)
	//db.Where("race_id = ? AND active =?", race_id, true).Find(&userRace)
	return result
}
