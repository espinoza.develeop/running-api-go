package models

import (
	"github.com/gofiber/fiber"
	"github.com/jinzhu/gorm"
	"github.com/miposdev/running-api-go/config/database"
)

type Book struct {
	gorm.Model
	Title  string `json:"title"`
	Author string `json:"author"`
	Rating int    `json:rating`
}

func GetBooks() []Book {
	db := database.DBconn
	var books []Book
	db.Find(&books)
	return books
	//c.JSON(books)
}

func GetBook(id uint64) Book {
	db := database.DBconn
	var book Book
	db.Find(&book, id)
	return book
}

func NewBook(c *fiber.Ctx) *Book {
	db := database.DBconn
	book := new(Book)
	c.BodyParser(book)
	db.Create(&book)
	return book
}

func DeleteBook(book Book) string {
	db := database.DBconn
	db.Delete(&book)
	return "ok"
}
