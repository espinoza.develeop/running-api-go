package controllers

import (
	"time"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber"
	jwtware "github.com/gofiber/jwt"
	"github.com/miposdev/running-api-go/models"
	"github.com/miposdev/running-api-go/utils"
)

func GetUserData(ctx *fiber.Ctx) *models.User {
	user := ctx.Locals("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	fmt.Println(claims)
	id := claims["id"].(float64)
	email := claims["email"].(string)
	deviceId := claims["deviceId"].(string)
	// deviceIdPhone := claims["deviceIdPhone"].(string)
	userFinal := new(models.User)
	userFinal.ID = uint(id)
	userFinal.Email = email
	userFinal.DeviceId = deviceId
	// userFinal.DeviceIdPhone = deviceIdPhone
	return userFinal
}

func AuthRequiered() func(ctx *fiber.Ctx) {
	var jwtToken = utils.ViperEnvVariable("JWT_SECRET")
	return jwtware.New(jwtware.Config{
		SigningKey: []byte(jwtToken),
		ErrorHandler: func(ctx *fiber.Ctx, err error) {
			ctx.Status(fiber.StatusUnauthorized).JSON(
				fiber.Map{
					"error": "Unauthorized",
				},
			)
		},
	})
}

func Login(ctx *fiber.Ctx) {

	user := new(models.User)
	err := ctx.BodyParser(&user)
	if err != nil {
		ctx.Status(fiber.StatusBadRequest).JSON(
			fiber.Map{
				"error": "cannot parse json",
			},
		)
		return
	}

	var email = user.Name
	var name  = user.Email

	var userFound = models.GetUserByAppleId(user.AppleIdentifier)
	var userId = userFound.ID
	if userFound.AppleIdentifier == "" {
		//Se crea el usuario
		user = models.NewUser(ctx)
		userId = user.ID
	} else {
		
		models.UpdateDevice(int(userId), user.DeviceId, user.DeviceIdPhone)
		
		if user.Email != ""{
			models.UpdateEmail(int(userId), user.Email)
		}
		email = userFound.Email
		name  = userFound.Name
	}
	//Se procede a generar el token para el usuario
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["id"] = userId
	claims["email"] = email
	claims["deviceId"] = user.DeviceId
	claims["exp"] = time.Now().Add(time.Hour * 24 * 360) //a week
	var jwtToken = utils.ViperEnvVariable("JWT_SECRET")
	s, err := token.SignedString([]byte(jwtToken))
	if err != nil {
		ctx.Send(err)
		return
	}
	ctx.Status(fiber.StatusOK).JSON(fiber.Map{
		"token": s,
		"user": struct {
			UserId int    `json:"user_id"`
			Email  string `json:"email"`
			Name   string `json:"name"`
		}{
			UserId: int(userId),
			Email:  email,
			Name:   name,
		},
	})
}

//setea el valor del token para notificaciones
func SetDevideId(c *fiber.Ctx) {
	//Se recibe el device_id para su actualizacion

	user := new(models.User)
	fmt.Println("llega:", user.DeviceId,user.DeviceIdPhone)
	if err := c.BodyParser(user); err != nil {
		c.Status(503).Send(err)
		return
	}
	//Se obtiene el id a partir del token
	var userInfo = GetUserData(c)
	var fullUserInfo = models.GetUserById(int(userInfo.ID))
	
	var newDeviceId = fullUserInfo.DeviceId
	var newDeviceIdPhone = fullUserInfo.DeviceIdPhone
	if user.DeviceId != "" {
		newDeviceId = user.DeviceId
	}
	if user.DeviceIdPhone != "" {
		newDeviceIdPhone = user.DeviceIdPhone
	}
	var userUpdated = models.UpdateDevice(int(userInfo.ID), newDeviceId, newDeviceIdPhone)
	fmt.Println("actualiza:", userUpdated.DeviceId,userUpdated.DeviceIdPhone)
	c.Status(fiber.StatusOK).JSON(userUpdated)
}

// obtiene el perfil de usuario
//setea el valor del token para notificaciones
func GetUserProfile(c *fiber.Ctx) {

	//Se obtiene el id a partir del token
	var userInfo = GetUserData(c)
	var fullUserInfo = models.GetUserById(int(userInfo.ID))
	
	
	c.Status(fiber.StatusOK).JSON(fiber.Map{
		"user": struct {
			Id          int             `json:"user_id"`
			Name      string            `json:"name"`
			Email      string          	`json:"email"`
			DeviceId      string        `json:"device_id"`
			DeviceIphoneId   string     `json:"device_iphone_id"`
			
		}{
			Id:          int(fullUserInfo.ID),
			Name:      fullUserInfo.Name,
			Email:      fullUserInfo.Email,
			DeviceId:      fullUserInfo.DeviceId,
			DeviceIphoneId:   fullUserInfo.DeviceIdPhone,
		},
	})
}