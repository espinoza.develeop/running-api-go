package controllers

import (
	"log"
	"strconv"

	"github.com/gofiber/fiber"
	"github.com/miposdev/running-api-go/models"
)

func GetBooks(c *fiber.Ctx) {
	var books = models.GetBooks()
	c.JSON(books)
}

func GetBook(c *fiber.Ctx) {
	id := c.Params("id")
	var finalId, err2 = strconv.ParseUint(id, 10, 32)
	if err2 != nil {
		log.Fatalf("Error while reading config file %s", err2)
	}
	var book = models.GetBook(finalId)
	c.JSON(book)
}

func NewBook(c *fiber.Ctx) {
	book := new(models.Book)
	if err := c.BodyParser(book); err != nil {
		c.Status(503).Send(err)
		return
	}
	var bookCreated = models.NewBook(c)
	c.JSON(bookCreated)
}

func DeleteBook(c *fiber.Ctx) {
	id := c.Params("id")
	var finalId, err2 = strconv.ParseUint(id, 10, 32)
	if err2 != nil {
		log.Fatalf("Error while reading config file %s", err2)
	}
	var book = models.GetBook(finalId)
	if book.Title == "" {
		c.Status(500).Send("No book found")
		return
	}
	models.DeleteBook(book)
	c.Send("Book succesfully deleted")
}
