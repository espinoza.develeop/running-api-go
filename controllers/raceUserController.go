package controllers

import (
	"time"

	"github.com/gofiber/fiber"
	"github.com/miposdev/running-api-go/models"
	"github.com/miposdev/running-api-go/utils"
)

func AddUserToRace(c *fiber.Ctx) {
	//Se recibe el codigo de la carrera race_id

	race := new(models.Race)
	if err := c.BodyParser(race); err != nil {
		c.Status(503).Send(err)
		return
	}

	var recoverRace = models.GetRaceByRamdomId(race.RaceId)
	if recoverRace.ID == 0 {
		c.Status(fiber.StatusBadRequest).JSON(
			fiber.Map{
				"error": "cannot find a race with the code " + race.RaceId,
			},
		)
		return
	}
	if recoverRace.Status != utils.RACE_CREATED {
		c.Status(fiber.StatusBadRequest).JSON(
			fiber.Map{
				"error": "The race is already started or finished",
			},
		)
		return
	}
	//Se obtiene el id a partir del token
	var userInfo = GetUserData(c)
	var fullUserInfo = models.GetUserById(int(userInfo.ID))
	//Se comprueba si ya existe una carrera ingresada para el usuario
	var validateRaceExist = models.GetUserRaceByUserId(int(userInfo.ID), int(recoverRace.ID))
	if validateRaceExist.ID != 0 && validateRaceExist.Active {
		c.Status(fiber.StatusBadRequest).JSON(
			fiber.Map{
				"error": "you are already part of the race " + race.RaceId,
			},
		)
		return
	}
	type userJoined struct {
		Fullname string  `json:"fullname"`
		UserId   int     `json:"user_id"`
		RaceId   int     `json:"race_id"`
		Km       float32 `json:"km"`
		Distance float32 `json:"distance"`
		Speed    float32 `json:"speed"`
	}

	if validateRaceExist.ID != 0 {
		// se procede a actualizar el active a true
		var raceUserUpdated = models.UpdateRaceUserByUserId(int(userInfo.ID), int(recoverRace.ID), true)
		userJoinedObj := new(userJoined)
		userJoinedObj.UserId = raceUserUpdated.UserId
		userJoinedObj.RaceId = raceUserUpdated.RaceId
		userJoinedObj.Fullname = fullUserInfo.Name
		userJoinedObj.Km = 0
		userJoinedObj.Distance = 0
		userJoinedObj.Speed = 0
		//Se recupera todos los usuarios de la carrera con su respectivo device id y se procede a notificar
		var raceMembers = models.GetUserRaceByRaceId(int(recoverRace.ID))
		for _, member := range raceMembers {
			//Notifica a todos los miembros de la carrera

			utils.ApplePushNotification(member.DeviceId, "user-joined", userJoinedObj, "watch")
		}

		c.Status(fiber.StatusOK).JSON(fiber.Map{
			"race": struct {
				Id          int             `json:"id"`
				UserId      int             `json:"user_id"`
				RaceId      string          `json:"race_id"`
				Status      string          `json:"status"`
				CreatedAt   time.Time       `json:"created_at"`
				UpdatedAt   time.Time       `json:"updated_at"`
				InitiatedAt time.Time       `json:"initiated_at"`
				Members     []models.Result `json:"members"`
			}{
				Id:          int(raceUserUpdated.ID),
				UserId:      recoverRace.UserId,
				RaceId:      recoverRace.RaceId,
				Status:      recoverRace.Status,
				CreatedAt:   recoverRace.CreatedAt,
				UpdatedAt:   recoverRace.UpdatedAt,
				InitiatedAt: recoverRace.InitiatedAt,
				Members:     raceMembers,
			},
		})
	} else {
		// Se crea el registro en la tabla race_user
		raceUser := new(models.RaceUser)
		raceUser.UserId = int(userInfo.ID)
		raceUser.RaceId = int(recoverRace.ID)
		raceUser.Active = true
		var raceUserCreated = models.NewRaceUser(raceUser)
		userJoinedObj := new(userJoined)
		userJoinedObj.UserId = raceUserCreated.UserId
		userJoinedObj.RaceId = raceUserCreated.RaceId
		userJoinedObj.Fullname = fullUserInfo.Name
		userJoinedObj.Km = 0
		userJoinedObj.Distance = 0
		userJoinedObj.Speed = 0
		//Se recupera todos los usuarios de la carrera con su respectivo device id y se procede a notificar
		var raceMembers = models.GetUserRaceByRaceId(int(recoverRace.ID))
		for _, member := range raceMembers {
			//Notifica a todos los miembros de la carrera
			utils.ApplePushNotification(member.DeviceId, "user-joined", userJoinedObj, "watch")
		}

		c.Status(fiber.StatusOK).JSON(fiber.Map{
			"race": struct {
				Id          int             `json:"id"`
				UserId      int             `json:"user_id"`
				RaceId      string          `json:"race_id"`
				Status      string          `json:"status"`
				CreatedAt   time.Time       `json:"created_at"`
				UpdatedAt   time.Time       `json:"updated_at"`
				InitiatedAt time.Time       `json:"initiated_at"`
				Members     []models.Result `json:"members"`
			}{
				Id:          int(raceUserCreated.ID),
				UserId:      recoverRace.UserId,
				RaceId:      recoverRace.RaceId,
				Status:      recoverRace.Status,
				CreatedAt:   recoverRace.CreatedAt,
				UpdatedAt:   recoverRace.UpdatedAt,
				InitiatedAt: recoverRace.InitiatedAt,
				Members:     raceMembers,
			},
		})
	}

}

func RemoveUserFromRace(c *fiber.Ctx) {
	//Se recibe el codigo de la carrera race_id

	race := new(models.Race)
	if err := c.BodyParser(race); err != nil {
		c.Status(503).Send(err)
		return
	}

	var recoverRace = models.GetRaceByRamdomId(race.RaceId)
	if recoverRace.ID == 0 {
		c.Status(fiber.StatusBadRequest).JSON(
			fiber.Map{
				"error": "cannot find a race with the code " + race.RaceId,
			},
		)
		return
	}
	if recoverRace.Status != utils.RACE_CREATED {
		c.Status(fiber.StatusBadRequest).JSON(
			fiber.Map{
				"error": "The race is already started or finished",
			},
		)
		return
	}
	//Se obtiene el id a partir del token
	var userInfo = GetUserData(c)

	//Se comprueba si ya existe una carrera ingresada para el usuario
	var validateRaceExist = models.GetUserRaceByUserId(int(userInfo.ID), int(recoverRace.ID))
	if validateRaceExist.ID == 0 {
		c.Status(fiber.StatusBadRequest).JSON(
			fiber.Map{
				"error": "you are not part of the race " + race.RaceId,
			},
		)
		return
	}

	var raceUserUpdated = models.UpdateRaceUserByUserId(int(userInfo.ID), int(recoverRace.ID), false)

	var raceMembers = models.GetUserRaceByRaceId(int(recoverRace.ID))
	for _, member := range raceMembers {
		//Notifica a todos los miembros de la carrera
		utils.ApplePushNotification(member.DeviceId, "user-quit", raceUserUpdated, "watch")
	}

	c.Status(fiber.StatusOK).JSON(fiber.Map{
		"race": struct {
			Id     int    `json:"id"`
			UserId int    `json:"user_id"`
			RaceId string `json:"race_id"`
		}{
			Id:     int(raceUserUpdated.ID),
			UserId: raceUserUpdated.UserId,
			RaceId: recoverRace.RaceId,
		},
	})
}

func IsUserInRace(c *fiber.Ctx) {
	
	//Se obtiene el id a partir del token
	var userInfo = GetUserData(c)

	var last_race_user = models.GetLastUserRaceByUserId(int(userInfo.ID))
	if last_race_user.ID == 0{
		c.Status(fiber.StatusBadRequest).JSON(
			fiber.Map{
				"error": "cannot find any race of the user",
			},
		)
		return
	}
	var race = models.GetRaceById(int(last_race_user.RaceId))
	

	c.Status(fiber.StatusOK).JSON(fiber.Map{
		"race": struct {
			Id     int    `json:"id"`
			UserId int    `json:"user_id"`
			RaceId string `json:"race_id"`
			Status string `json:"status"`
		}{
			Id:     int(race.ID),
			UserId: race.UserId,
			RaceId: race.RaceId,
			Status: race.Status,
		},
	})
}
