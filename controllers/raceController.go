package controllers

import (
	"encoding/json"
	"fmt"
	"math"
	"math/rand"
	"strconv"
	"time"

	"github.com/gofiber/fiber"
	"github.com/miposdev/running-api-go/models"
	"github.com/miposdev/running-api-go/utils"
)

func NewRace(c *fiber.Ctx) {
	race := new(models.Race)
	if err := c.BodyParser(race); err != nil {
		c.Status(503).Send(err)
		return
	}
	//Se obtiene el id a partir del token
	var userInfo = GetUserData(c)
	//Se procede a crear un numero aleatorio de 4 digitos y se concatena el id del user
	var numberIdentifier = randomNum()
	var numberIdentifierString = strconv.Itoa(numberIdentifier)
	numberIdentifierString += strconv.Itoa(int(userInfo.ID))
	//Se obtiene el id del usuario que esta intentando crear la carrera
	race.RaceId = numberIdentifierString
	race.Active = true
	if race.Status == utils.RACE_CREATED{
		dt := time.Now()
		race.EnabledAt = dt 
	}
	// race.Status = "C"
	race.UserId = int(userInfo.ID)
	var raceCreated = models.NewRace(race)

	// Se crea el registro en la tabla race_user
	raceUser := new(models.RaceUser)
	raceUser.UserId = raceCreated.UserId
	raceUser.RaceId = int(raceCreated.ID)
	raceUser.Active = true
	var raceUserCreated = models.NewRaceUser(raceUser)

	c.Status(fiber.StatusOK).JSON(fiber.Map{
		"race": struct {
			Id        int       `json:"id"`
			UserId    int       `json:"user_id"`
			RaceId    string    `json:"race_id"`
			CreatedAt time.Time `json:"created_at"`
		}{
			Id:        int(raceCreated.ID),
			UserId:    raceUserCreated.UserId,
			RaceId:    raceCreated.RaceId,
			CreatedAt: raceCreated.CreatedAt,
		},
	})
}

func ChangeRaceStatus(c *fiber.Ctx) {
	race := new(models.Race)
	if err := c.BodyParser(race); err != nil {
		c.Status(503).Send(err)
		return
	}
	//var userInfo = GetUserData(c)

	var raceUpdate = models.UpdateRaceByRamdomId(race.RaceId, race.Status)

	//Se recupera todos los usuarios de la carrera con su respectivo device id y se procede a notificar
	var raceMembers = models.GetUserRaceByRaceId(int(raceUpdate.ID))
	for _, member := range raceMembers {
		//Notifica a todos los miembros de la carrera
		utils.ApplePushNotification(member.DeviceId, "race-change-status", raceUpdate, "watch")

		if race.Status == "E" {
			utils.ApplePushNotification(member.DeviceIdPhone, "Finalización de carrera", raceUpdate, "phone" )
		}
	}

	c.Status(fiber.StatusOK).JSON(raceUpdate)
}

func SaveRaceLocation(c *fiber.Ctx) {
	raceData := new(models.RaceData)

	race := new(models.Race)
	if err := c.BodyParser(raceData); err != nil {
		c.Status(503).Send(err)
		return
	}
	if err := c.BodyParser(race); err != nil {
		c.Status(503).Send(err)
		return
	}

	if raceData.Longitude == 0 || raceData.Latitude == 0 {
		type MessageResponse struct {
			Message string `json:"message"`
		}
		message := new(MessageResponse)
		message.Message = "valores incorrectos"
		c.Status(fiber.StatusOK).JSON(message)
		return
	}
	var userInfo = GetUserData(c)

	var raceInfo = models.GetRaceByRamdomId(race.RaceId)
	//VALIDAR CUANDO NO ENCUENTRE EL RACE
	if raceInfo.ID == 0 {
		c.Status(fiber.StatusBadRequest).JSON(
			fiber.Map{
				"error": "Race " + raceInfo.RaceId + " not found",
			},
		)
		return
	}

	//Se obtiene el user_race con el race_id y el user_id
	var userRace = models.GetUserRaceByUserId(int(userInfo.ID), int(raceInfo.ID))
	var userObj = models.GetUserById(int(userInfo.ID))
	//Se obtiene la ultima raceData
	// var raceDataRetrieved = models.GetRaceDataLast(int(userRace.ID))

	// if len(raceDataRetrieved) > 0 {

	// 	//Se obtiene la distancia usando los datos del registro anterior con el de los nuevos
	// 	var raceDataLast = raceDataRetrieved[0]
	// 	fmt.Println(raceData.Latitude)
	// 	raceData.Distance = distance(raceDataLast.Latitude, raceDataLast.Longitude, raceData.Latitude, raceData.Longitude, "K") + raceDataLast.Distance
	// } else {
	// 	//Al no tener mas registros en la bd la distancia sera 0
	// 	raceData.Distance = 0
	// }
	//Se ingresa la información en la tabla raceData
	raceData.Active = true
	raceData.RaceUserId = int(userRace.ID)
	var raceDataCreated = models.NewRaceData(raceData)
	//utils.ApplePushNotification(userInfo.DeviceId, "race-data-info", raceDataCreated)
	//Se recupera todos los usuarios de la carrera con su respectivo device id y se procede a notificar
	var raceMembers = models.GetUserRaceByRaceId(int(userRace.RaceId))
	for _, member := range raceMembers {
		//Notifica a todos los miembros de la carrera
		type RaceDataUser struct {
			RaceUserId int     `json:"race_user_id"`
			Latitude   float64 `json:"latitude"`
			Longitude  float64 `json:"longitude"`
			Speed      float32 `json:"speed"`
			Altitude   float32 `json:"altitude"`
			HeartRate  float32 `json:"heart_rate"`
			Distancia  float64 `json:"distancia"`
			Active     bool    `json:"active"`
			UserId     int     `json:"user_id"`
			Fullname   string  `json:"fullname"`
			RaceStatus string  `json:"race_status"`
		}
		raceUserData := new(RaceDataUser)
		raceUserData.RaceUserId = raceDataCreated.RaceUserId
		raceUserData.Latitude = raceDataCreated.Latitude
		raceUserData.Longitude = raceDataCreated.Longitude
		raceUserData.Speed = raceDataCreated.Speed
		raceUserData.Altitude = raceDataCreated.Altitude
		raceUserData.HeartRate = raceDataCreated.HeartRate
		raceUserData.Distancia = raceDataCreated.Distance
		raceUserData.Active = raceDataCreated.Active
		raceUserData.UserId = userRace.UserId
		raceUserData.Fullname = userObj.Name
		raceUserData.RaceStatus = raceInfo.Status
		fmt.Println("raceUserData", raceUserData)

		utils.ApplePushNotification(member.DeviceId, "race-data-info", raceUserData, "watch")
	}
	c.Status(fiber.StatusOK).JSON(raceDataCreated)
}

func GetRaceDataAndMembers(c *fiber.Ctx) {
	race := new(models.Race)
	if err := c.BodyParser(race); err != nil {
		c.Status(503).Send(err)
		return
	}
	var userInfo = GetUserData(c)
	c.Status(fiber.StatusOK).JSON(userInfo)
	var raceInfo = models.GetRaceByRamdomId(race.RaceId)
	//VALIDAR CUANDO NO ENCUENTRE EL RACE
	if raceInfo.ID == 0 {
		c.Status(fiber.StatusBadRequest).JSON(
			fiber.Map{
				"error": "Race " + raceInfo.RaceId + " not found",
			},
		)
		return
	}
	//Se obtiene los usuarios de la carrera
	var usersRace = models.GetUserRaceByRaceId(int(raceInfo.ID))

	
	type ResultFinal struct {
		RaceId  string          `json:"race_id"`
		Status  string          `json:"status"`
		Active  bool            `json:"active"`
		Members []models.Result `json:"members"`
	}

	var result ResultFinal
	result.RaceId = raceInfo.RaceId
	result.Active = raceInfo.Active
	result.Status = raceInfo.Status
	result.Members = usersRace

	b, err := json.Marshal(result)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(b))

	c.Status(fiber.StatusOK).JSON(result)
}

func GetHistoryRaceDataAndMembers(c *fiber.Ctx) {

	//obtiene el historial de 10 carreras pasadas.

	// race := new(models.Race)
	// if err := c.BodyParser(race); err != nil {
	// 	c.Status(503).Send(err)
	// 	return
	// }
	var userInfo = GetUserData(c)
	c.Status(fiber.StatusOK).JSON(userInfo)

	var history_races = models.GetTenRaceByUserId(int(userInfo.ID))
	
	type ResultRace struct {
		RaceId  string          `json:"race_id"`
		Status  string          `json:"status"`
		Active  bool            `json:"active"`
		Members []models.Result `json:"members"`
	}

	type ResultFinal struct{
		Races []ResultRace `json:"races"`
	}

	
	var result_races []ResultRace 

	for _, race := range history_races {
		var race_id = race.ID

		//Se obtiene los usuarios de la carrera
		var usersRace = models.GetUserRaceByRaceId(int(race_id))

		var result ResultRace
		result.RaceId = race.RaceId
		result.Active = race.Active
		result.Status = race.Status
		result.Members = usersRace

		result_races = append(result_races, result)
	}

	var resultFinal ResultFinal
	resultFinal.Races = result_races


	b, err := json.Marshal(resultFinal)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(b))

	c.Status(fiber.StatusOK).JSON(resultFinal)
}

func randomNum() int {
	//semilla a nivel nanosegundo en unix time
	rand.Seed(time.Now().UTC().UnixNano())

	return rand.Intn(9999-1000) + 1000
}

//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:::                                                                         :::
//:::  This routine calculates the distance between two points (given the     :::
//:::  latitude/longitude of those points). It is being used to calculate     :::
//:::  the distance between two locations using GeoDataSource (TM) products  :::
//:::                                                                         :::
//:::  Definitions:                                                           :::
//:::    South latitudes are negative, east longitudes are positive           :::
//:::                                                                         :::
//:::  Passed to function:                                                    :::
//:::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
//:::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
//:::    unit = the unit you desire for results                               :::
//:::           where: 'M' is statute miles (default)                         :::
//:::                  'K' is kilometers                                      :::
//:::                  'N' is nautical miles                                  :::
//:::                                                                         :::
//:::  Worldwide cities and other features databases with latitude longitude  :::
//:::  are available at https://www.geodatasource.com                         :::
//:::                                                                         :::
//:::  For enquiries, please contact sales@geodatasource.com                  :::
//:::                                                                         :::
//:::  Official Web site: https://www.geodatasource.com                       :::
//:::                                                                         :::
//:::               GeoDataSource.com (C) All Rights Reserved 2018            :::
//:::                                                                         :::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

func distance(lat1 float64, lng1 float64, lat2 float64, lng2 float64, unit ...string) float64 {
	const PI float64 = 3.141592653589793

	radlat1 := float64(PI * lat1 / 180)
	radlat2 := float64(PI * lat2 / 180)

	theta := float64(lng1 - lng2)
	radtheta := float64(PI * theta / 180)

	dist := math.Sin(radlat1)*math.Sin(radlat2) + math.Cos(radlat1)*math.Cos(radlat2)*math.Cos(radtheta)

	if dist > 1 {
		dist = 1
	}

	dist = math.Acos(dist)
	dist = dist * 180 / PI
	dist = dist * 60 * 1.1515

	if len(unit) > 0 {
		if unit[0] == "K" {
			dist = dist * 1.609344
		} else if unit[0] == "N" {
			dist = dist * 0.8684
		}
	}

	return dist
}

func CalculateDistanceTest(c *fiber.Ctx) {
	type DataInicial struct {
		Lat1     float64 `json:"lat_1"`
		Lng1     float64 `json:"lng_1"`
		Lat2     float64 `json:"lat_2"`
		Lng2     float64 `json:"lng_2"`
		Distance float64 `json:"distance"`
	}
	dataInicial := new(DataInicial)
	if err := c.BodyParser(dataInicial); err != nil {
		c.Status(503).Send(err)
		return
	}
	var distance = distance(dataInicial.Lat1, dataInicial.Lng1, dataInicial.Lat2, dataInicial.Lng2, "K")
	dataInicial.Distance = distance
	c.Status(fiber.StatusOK).JSON(dataInicial)
}
