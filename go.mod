module github.com/miposdev/running-api-go

go 1.15

require (
	github.com/anachronistic/apns v0.0.0-20151129191123-91763352f7bf
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/edganiukov/apns v0.2.1
	github.com/gofiber/fiber v1.14.6
	github.com/gofiber/jwt v0.2.0
	github.com/jinzhu/gorm v1.9.16
	github.com/sideshow/apns2 v0.20.0
	github.com/spf13/viper v1.7.1
	github.com/streadway/amqp v1.0.0
)
