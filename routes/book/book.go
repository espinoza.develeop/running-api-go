package book

import (
	"github.com/miposdev/running-api-go/controllers"

	"github.com/gofiber/fiber"
)

func SetUpRoutes(app *fiber.App) {
	app.Get("/api/v1/book", controllers.AuthRequiered(), controllers.GetBooks)
	app.Get("/api/v1/book/:id", controllers.AuthRequiered(), controllers.GetBook)
	app.Post("/api/v1/book", controllers.AuthRequiered(), controllers.NewBook)
	app.Delete("/api/v1/book/:id", controllers.AuthRequiered(), controllers.DeleteBook)
}
