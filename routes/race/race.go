package race

import (
	"github.com/miposdev/running-api-go/controllers"

	"github.com/gofiber/fiber"
)

func SetUpRoutes(app *fiber.App) {
	app.Post("/api/v1/race", controllers.AuthRequiered(), controllers.NewRace)
	app.Post("/api/v1/race-add-user", controllers.AuthRequiered(), controllers.AddUserToRace)
	app.Post("/api/v1/race-remove-user", controllers.AuthRequiered(), controllers.RemoveUserFromRace)
	app.Post("/api/v1/race-change-status", controllers.AuthRequiered(), controllers.ChangeRaceStatus)
	app.Post("/api/v1/race-save-locations", controllers.AuthRequiered(), controllers.SaveRaceLocation)
	app.Post("/api/v1/race-members-info", controllers.AuthRequiered(), controllers.GetRaceDataAndMembers)
	app.Get("/api/v1/race-user", controllers.AuthRequiered(), controllers.IsUserInRace)
	app.Post("/api/v1/calculate-distance", controllers.CalculateDistanceTest)
	app.Get("/api/v1/race-history", controllers.AuthRequiered(), controllers.GetHistoryRaceDataAndMembers)
	
}
