package user

import (
	"github.com/miposdev/running-api-go/controllers"

	"github.com/gofiber/fiber"
)

func SetUpRoutes(app *fiber.App) {
	app.Post("/api/v1/login", controllers.Login)
	app.Post("/api/v1/change-device-id", controllers.AuthRequiered(), controllers.SetDevideId)
	app.Get("/api/v1/get-profile", controllers.AuthRequiered(), controllers.GetUserProfile)

}
