package routes

import (
	"github.com/gofiber/fiber"
	"github.com/miposdev/running-api-go/routes/book"
	"github.com/miposdev/running-api-go/routes/race"
	"github.com/miposdev/running-api-go/routes/user"
)

func SetUpRoutes(app *fiber.App) {
	book.SetUpRoutes(app)
	user.SetUpRoutes(app)
	race.SetUpRoutes(app)
}
