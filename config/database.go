package config

import (
	"fmt"
	"log"
	"net/url"
	"strconv"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/miposdev/running-api-go/config/database"
	"github.com/miposdev/running-api-go/models"
	"github.com/miposdev/running-api-go/utils"
)

func InitDatabase() {
	var err error
	var host = utils.ViperEnvVariable("DB_HOST")
	var port = utils.ViperEnvVariable("DB_PORT")
	var finalPort, err2 = strconv.ParseUint(port, 10, 32)
	if err2 != nil {
		log.Fatalf("Error while reading config file %s", err2)
	}
	var user = utils.ViperEnvVariable("DB_USER")
	var pass = utils.ViperEnvVariable("DB_PASS")
	var dbName = utils.ViperEnvVariable("DB_NAME")
	dsn := url.URL{
		User:     url.UserPassword(user, pass),
		Scheme:   "postgres",
		Host:     fmt.Sprintf("%s:%d", host, finalPort),
		Path:     dbName,
		RawQuery: (&url.Values{"sslmode": []string{"disable"}}).Encode(),
	}
	database.DBconn, err = gorm.Open("postgres", dsn.String())
	//database.DBconn, err = gorm.Open("postgres", "books.db")

	if err != nil {
		panic(err)
	}
	fmt.Println("Database succesfull connected")
	database.DBconn.AutoMigrate(&models.Book{}, &models.User{}, &models.Race{}, &models.RaceUser{}, &models.RaceData{})
	//relations
	database.DBconn.Model(&models.Race{}).AddForeignKey("user_id", "users(id)", "RESTRICT", "RESTRICT")
	database.DBconn.Model(&models.RaceUser{}).AddForeignKey("user_id", "users(id)", "RESTRICT", "RESTRICT")
	database.DBconn.Model(&models.RaceUser{}).AddForeignKey("race_id", "races(id)", "RESTRICT", "RESTRICT")
	database.DBconn.Model(&models.RaceData{}).AddForeignKey("race_user_id", "race_users(id)", "RESTRICT", "RESTRICT")
	fmt.Println("Database succesfull migrated")

}
