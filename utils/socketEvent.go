package utils

import (
	"fmt"
	"log"

	"encoding/json"

	//apns "github.com/anachronistic/apns"
	//"github.com/edganiukov/apns"
	"github.com/sideshow/apns2"
	"github.com/sideshow/apns2/token"
	"github.com/streadway/amqp"
)

var rabbitPort = ViperEnvVariable("RABBITMQ_PORT")
var rabbitUsername = ViperEnvVariable("RABBITMQ_USERNAME")
var rabbitPass = ViperEnvVariable("RABBITMQ_PASSWORD")
var rabbitHost = ViperEnvVariable("RABBITMQ_HOST")
var rabbitVirtualHost = ViperEnvVariable("RABBITMQ_VHOST")

var authKeyPath = ViperEnvVariable("AUTHKEYPATH")
var keyId = ViperEnvVariable("KEYID")
var teamId = ViperEnvVariable("TEAMID")
var topic = ViperEnvVariable("TOPIC")
var topic_phone = ViperEnvVariable("TOPIC_PHONE")

//rabbit
func SendMessage(qname string, eventName string, message interface{}) {
	e, err := json.Marshal(message)
	type data struct {
		EventName string `json:"event_name"`
		Data      string `json:"data"`
	}
	dataObj := new(data)
	dataObj.EventName = eventName
	dataObj.Data = string(e)

	finalMessage, err := json.Marshal(dataObj)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(e))
	fmt.Println(rabbitVirtualHost)
	conn, err := amqp.Dial("amqp://" + rabbitUsername + ":" + rabbitPass + "@" + rabbitHost + ":" + rabbitPort + "/" + rabbitVirtualHost)

	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	defer conn.Close()

	fmt.Println("Successfully connected to rabbitmq instance")

	//creación de channel
	ch, err := conn.Channel()

	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	defer ch.Close()

	q, err := ch.QueueDeclare(
		qname, // name
		false, // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	fmt.Println("connected to ", q.Name)

	//body := "Hello World!"
	err = ch.Publish(
		"",    // exchange
		qname, // routing key
		false, // mandatory
		false, // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(string(finalMessage)),
		})
	if err != nil {
		fmt.Println(err)
		panic(err)
	}

	fmt.Println("Successfully published Message to queue")

}

func ApplePushNotification(deviceId string, eventName string, message interface{}, phone string) {
	e, err := json.Marshal(message)
	type data struct {
		EventName string `json:"event_name"`
		Data      string `json:"data"`
	}
	dataObj := new(data)
	dataObj.EventName = eventName
	dataObj.Data = string(e)

	// finalMessage, err := json.Marshal(dataObj)
	// if err != nil {
	// 	fmt.Println(err)
	// 	return
	// }
	// fmt.Println(string(e))
	authKey, err := token.AuthKeyFromFile(authKeyPath)
	if err != nil {
		log.Fatal("token error:", err)
	}

	token := &token.Token{
		AuthKey: authKey,
		// KeyID from developer account (Certificates, Identifiers & Profiles -> Keys)
		KeyID: keyId,
		// TeamID from developer account (View Account -> Membership)
		TeamID: teamId,
	}

	notification := &apns2.Notification{}
	notification.DeviceToken = deviceId
	if phone == "phone"{
		notification.Topic = topic_phone
	}else{
		notification.Topic = topic
	}
	
	notification.Payload = []byte(`{"aps":{"alert":"` + eventName + `"},"event":{"event_name":"` + eventName + `","data":` + string(e) + `}}`) // See Payload section below
	// notification.Payload = []byte(`{"aps":{"alert":` + string(finalMessage) + `},"event":` + string(finalMessage) + `}`) // See Payload section below
	fmt.Println(`{"aps":{"alert":"` + eventName + `"},"event":{"event_name":"` + eventName + `","data":` + string(e) + `}}`)
	// If you want to test push notifications for builds running directly from XCode (Development), use
	// client := apns2.NewClient(cert).Development()
	// For apps published to the app store or installed as an ad-hoc distribution use Production()

	client := apns2.NewTokenClient(token).Production()
	// client := apns2.NewTokenClient(token).Development()
	res, err := client.Push(notification)

	if err != nil {
		log.Fatal("Error:", err)
	}

	fmt.Printf("%v %v %v %v %v %v\n", deviceId, res.StatusCode, res.ApnsID, res.Reason, notification.Topic, phone )
}
