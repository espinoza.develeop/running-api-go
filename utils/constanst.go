package utils

const RACE_CREATED = "C"
const RACE_INITIATED = "I"
const RACE_END = "E"
const RACE_PAUSE = "P"
const RACE_SCHEDULED = "S"
