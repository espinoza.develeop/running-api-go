package main

import (
	"github.com/miposdev/running-api-go/routes"

	"github.com/gofiber/fiber"
	"github.com/gofiber/fiber/middleware"
	"github.com/miposdev/running-api-go/config"
	"github.com/miposdev/running-api-go/utils"
)

func helloWorld(c *fiber.Ctx) {
	//Recover user informatin from token
	c.Send("Hello, World")
}

func setUpRoutes(app *fiber.App) {
	routes.SetUpRoutes(app)
}

func main() {
	app := fiber.New()
	app.Use(middleware.Logger())
	setUpRoutes(app)
	app.Get("/hello", helloWorld)
	config.InitDatabase()
	var port = utils.ViperEnvVariable("APP_PORT")

	//rabbit
	//utils.SendMessage("running-events")

	app.Listen(port)
}
